﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

	static AudioManager _instance;
	static AudioManager instance{
		get{
			if (_instance == null) {
				_instance = new GameObject ("AudioManager", typeof(AudioManager)).GetComponent<AudioManager> ();
			}
			return _instance;
		}
	}



	public static void PlayLooping(string soundName, float volume){
		instance.instancePlayLooping (soundName, volume);
	}

	private void instancePlayLooping(string soundName, float volume){
		AudioSource source = gameObject.AddComponent<AudioSource>();
		source.loop = true;
		source.clip = Resources.Load<AudioClip>(soundName);
		source.volume = volume;
		source.Play();
	}

	private static List<AudioSource> availableSources = new List<AudioSource>();

	public static void PlaySound(string soundName, float volume){
		AudioSource source;
		int x = 0;
		do {
			if (x >= availableSources.Count) {
				source = instance.gameObject.AddComponent<AudioSource> ();
			} else {
				source = availableSources [x];
			}
		} while (!source.isPlaying);
		source.loop = false;
		source.clip = Resources.Load<AudioClip> (soundName);
		source.volume = volume;
		source.Play ();
	}

}
