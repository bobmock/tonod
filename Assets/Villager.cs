﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Villager {

	public string name;
	public bool isDead;
	public List<Belief> believes;
	public Charm[] charms;
	public List<ShareRecord> records = new List<ShareRecord> ();


	public Villager(string name){
		this.name = name;
		believes = new List<Belief> ();
		foreach (Charm[] charmArray in BroadRunner.instance.allCharms) {
			foreach (Charm charm in charmArray) {
				believes.Add(new Belief(5f, charm, 0f));
			}
		}
		ReselectCharms (BroadRunner.instance.allCharms);
	}

	public Belief selectShare(){
		List<int> lotteryTickets = new List<int> ();
		for (int i = 0; i < believes.Count; i++){
			lotteryTickets.Add(i);
			for (int j = 0; j < believes[i].fervor; j++){
				lotteryTickets.Add(i);
			}
		}
		Belief result = believes [lotteryTickets [Random.Range (0, lotteryTickets.Count)]];
		if (result.fervor > 0) {result.fervor--;}
		return result;
     }

	public float totalConfidence(int slot){
		float result = 0f;
		foreach (Charm charm in BroadRunner.instance.allCharms[slot]) {
			result += getBelief(charm).positivity;
		}
		return result;
	}

	public Belief getBelief(Charm charm){
		foreach (Belief belief in believes){
			if (belief.charm == charm){
				return belief;
			}
		}
		throw new UnityException("Not a belief!");
	}

	public void ReselectCharms(Charm[][] allCharms){
//		foreach (Charm charm in charms) {
//			getBelief (charm).positivity = 10f - (10f - getBelief (charm).positivity) / 1.05f;
//		}
		charms = new Charm[3];
		for (int i = 0; i < 3; i++) {
			float confidence = totalConfidence (i);
			int j = 0;
			while (charms[i] == null) {
				if (Random.Range (0f, confidence) < getBelief (allCharms [i] [j]).positivity) {//if this throws indexOutOfRange, my probability math is wrong
					charms [i] = allCharms [i] [j];
				} else {
					j++;
					confidence -= getBelief (allCharms [i] [j]).positivity;
				}
			}
		}
	}


}
