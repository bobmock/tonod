﻿using UnityEngine;
using System.Collections;

public class Belief {

	public float positivity;
	public Charm charm;
	public float fervor;

	public Belief(float positivity, Charm charm, float shared) {
		this.positivity = positivity;
		this.charm = charm;
		this.fervor = shared;
	}
}
