﻿using UnityEngine;
using System.Collections;

public class ShareRecord {

	public ShareRecord(Villager shared, Charm recd, float posit){
		sharedWith = shared;
		recommended = recd;
		positivity = posit;
	}

	public Villager sharedWith;
	public Charm recommended;
	public float positivity;
}
