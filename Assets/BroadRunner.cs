using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BroadRunner : MonoBehaviour
{

	public const int NUM_CHARMS = 5;
	public const int NUM_VILLAGERS = 10;
	private string lastCommand = null;
	public string toPrint;
	TextAdventureManager manager;
	public List<Villager> villagers;
	private string playerName;
	private Villager player;
	private Charm[] realCharmSet;
	public Charm[][] allCharms;
	public static BroadRunner instance;

	void Awake ()
	{
		instance = this;
	}

	void Start ()
	{
		manager = gameObject.GetComponent<TextAdventureManager> ();
		StartCoroutine (mainThread ());
	}

	public void takeNewCommand (string newCommand)
	{
		lastCommand = newCommand.ToLower ();
	}

	public Coroutine waitForCommand ()
	{
		return StartCoroutine (waitForCommandRoutine ());
	}

	public IEnumerator waitForCommandRoutine ()
	{
		while (lastCommand == null) {
			yield return null;
		}
	}

	public IEnumerator mainThread ()
	{
		GenerateTown ();
		AudioManager.PlayLooping ("sounds_ToDoR_bgm", .5f);
		yield return StartCoroutine (Intro ());
		yield return StartCoroutine (Night ());
		while (!CheckGameOver()) {
			yield return StartCoroutine (Day ());
			yield return StartCoroutine (Night ());
		}
		yield return GameOver ();
	}

	void GenerateTown ()
	{

		allCharms = buildCharmSet ();	// safe

		//populate villagers and charms array. Charms first, so villagers know to have them.
		villagers = new List<Villager> ();
		for (int i = 0; i < NUM_VILLAGERS; i++) {
			villagers.Add (formBaby ());
		}
	}

	Charm[][] buildCharmSet ()
	{
		HashSet<int> uniqueNums = new HashSet<int> ();
		Charm[][] charmSet = new Charm[3][];
		for (int i = 0; i < charmSet.Length; i++) {
			charmSet [i] = new Charm[NUM_CHARMS];
		}
		while (uniqueNums.Count<NUM_CHARMS) {
			uniqueNums.Add (Random.Range (0, doorCharms.Length));
		}
		for (int n=0; n<NUM_CHARMS; n++) {
			charmSet [0] [n] = new Charm (doorCharms [n]);
			charmSet [0] [n].charmType = 0;
		}
		uniqueNums.Clear ();
		while (uniqueNums.Count<NUM_CHARMS) {
			uniqueNums.Add (Random.Range (0, bodyCharms.Length));
		}
		for (int n=0; n<NUM_CHARMS; n++) {
			charmSet [1] [n] = new Charm (bodyCharms [n]);
			charmSet [1] [n].charmType = 1;
		}
		uniqueNums.Clear ();
		while (uniqueNums.Count<NUM_CHARMS) {
			uniqueNums.Add (Random.Range (0, eatCharms.Length));
		}
		for (int n=0; n<NUM_CHARMS; n++) {
			charmSet [2] [n] = new Charm (eatCharms [n]);
			charmSet [2] [n].charmType = 2;
		}
		return charmSet;
	}
	
	Villager formBaby ()
	{
		Villager baby = new Villager (firstNames [Random.Range (0, firstNames.Length)] + " " + lastNames [Random.Range (0, lastNames.Length)]);
		baby.ReselectCharms (allCharms);
		return baby;
	}

	public bool CheckGameOver ()
	{
		return (hunger == 3 || villagersRemaining == 0);
	}

	public string TakeCommand ()
	{
		string result = lastCommand;
		lastCommand = null;
		return result;
	}

	public void QueueUpdate (string newText)
	{

		toPrint += "\n" + newText;
		if (newText.Length > 1000) {
			toPrint = toPrint.Substring (toPrint.Length - 1000);
		}
		manager.OnFinishedUpdate ();
	}

	public void SendUpdate (string newText)
	{//make this all one with upwards console scrolling?
		toPrint += "\n\n";
		QueueUpdate (newText);
		manager.OnFinishedUpdate ();
	}

	public IEnumerator Intro ()
	{
		SendUpdate ("Hello! Welcome to a game! What's yer name?");
		yield return waitForCommand ();
		playerName = TakeCommand ();
		player = new Villager (playerName);
		SendUpdate ("Your breath comes as painful, ragged gasps: each a desired to breathe no more.");
		QueueUpdate ("Every beat of your heart betrays you, sending boiling blood coursing through you like rivers of molten lead.");
		SendUpdate ("Your teeth bare sharp, cutting through your pallid gums and leaving behind the ferric taste of blood in your mouth.");
		SendUpdate ("Every torturous pang reminds you of the gnawing Hunger within. But beneath the Hunger lies your cunning, a desire for survival. A feast of your own design...");
		SendHelp ("<Press Enter>");
		yield return waitForCommand ();
		TakeCommand ();
	}
	
	public IEnumerator Day ()
	{
		SendUpdate ("You enter town, the bustle of daily life hides well the nighttime fears.");
		string[] splitCommand = new string[]{"talk"};
		currentHouse = -1;
		DayStep ();	// does daystep get called 3 times?
		for (int i = 0; i < 3; i++) {
			bool dayFlag = false;
			do {
				SendHelp ("look\ntalk\neavesdrop");
				string thiscommand;
				do {
					yield return waitForCommand ();
					thiscommand = TakeCommand ();
				} while (thiscommand == "");
				splitCommand = thiscommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
				switch (splitCommand [0]) {
				case "look":
					LookHouses ();
					break;
				case "talk":
					if (!isExiled) {
						QueueUpdate ("Who do you want to talk to?");
						LookHouses ();
						SendHelp ("Enter house #");
						string thatcommand;
						do {
							yield return waitForCommand ();
							thatcommand = TakeCommand ();
						} while (thatcommand == "");
						splitCommand = thatcommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
						currentHouse = IdentifyHouse (splitCommand [0]) - 1;
						//talk
						SendUpdate ("You approach " + villagers [currentHouse].name + ".");
						Talk (villagers [currentHouse]);
						//accept ask or recommend
						bool talkFlag = false;
						SendUpdate ("You could ask them about what protective measures they've taken, or you could spread a little rumor...");
						do {
							SendHelp ("ask\nrecommend");
							string thecommand;
							do {
								yield return waitForCommand ();
								thecommand = TakeCommand ();
							} while (thecommand == "");
							string askRecommendCommand = TakeCommand ();
							string[] splitRecCommand = thecommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
							switch (splitRecCommand [0]) {
							case "ask":
								bool askFlag = false;
								do {
									QueueUpdate ("Ask about which charm?");
									SendHelp ("door\nbody\nfood");
									string thoucommand;
									do {
										yield return waitForCommand ();
										thoucommand = TakeCommand ();
									} while (thoucommand == "");									
									splitRecCommand = thoucommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
									switch (splitRecCommand [0]) {
									case "door":
										string[] doorResponses = new string[]
										{	"I've been putting a " + villagers [currentHouse].charms [0].myname + " over my door at night, keeps the devils at bay.",
											"Tacked up a " + villagers [currentHouse].charms [0].myname + " over my door, so far haven't had much trouble!",
											"Hang up a " + villagers [currentHouse].charms [0].myname + ", and you'll wake up alive and well.",
									"It's a little unnerving, but maybe i'll put a " + villagers [currentHouse].charms [0].myname + " up on the door.",
											"Do you think a " + villagers [currentHouse].charms [0].myname + " above the door would work? Worth a try, i'd say.",
											"The beast is probably like those legends, so a " + villagers [currentHouse].charms [0].myname + " would stop it, right?",
											"Isn't " + villagers [currentHouse].charms [0].myname + " just a fairy tale?",
											"Why a person would hang up a " + villagers [currentHouse].charms [0].myname + " on their door is just beyond me!",
											"I don't think that " + villagers [currentHouse].charms [0].myname + " has worked in the past, nor will it tonight.",
								};
										ask (doorResponses, 0);
										askFlag = true;
										break;	// case door break
									case "body":
										string[] bodyResponses = new string[]
										{	"I always keep " + villagers [currentHouse].charms [1].myname + " on my person when I hit the hay.",
											"Takin' to keeping a " + villagers [currentHouse].charms [1].myname + " close with me when I sleep.",
											"I never close my eyes without my " + villagers [currentHouse].charms [1].myname + " at hand.",
											"Maybe if I had a " + villagers [currentHouse].charms [1].myname + " with me, i'd sleep safe.",
											"Devils can't touch you if you wear a " + villagers [currentHouse].charms [1].myname + ", right?",
											"A " + villagers [currentHouse].charms [1].myname + " keeps the beast at bay, or so I've heard...",
											"I can't imagine holding " + villagers [currentHouse].charms [1].myname + " while I sleep!",
									"Isn't " + villagers [currentHouse].charms [1] + " just a superstition?",
											"I'm hearing rumors that " + villagers [currentHouse].charms [1].myname + " doesn't actually work."
								};
										ask (bodyResponses, 1);
										askFlag = true;
										break;	// case body break
									case "food":
										string[] foodResponses = new string[]
								{	"A nightly meal of " + villagers [currentHouse].charms [2].myname + ", so I don't end up one myself!",
									"Just consume some " + villagers [currentHouse].charms [2].myname + " before you rest your head, and you'll be just fine!",
											"Take " + villagers [currentHouse].charms [2].myname + " with your supper, that'll stave off the devils.",
											"Some things, like " + villagers [currentHouse].charms [2].myname + ", are said to prevent devils from getting too close.",
											"I've never considered eating " + villagers [currentHouse].charms [2].myname + " before I sleep. Does it work?",
											"Doesn't " + villagers [currentHouse].charms [2].myname + " have some sort of essence that repels demons?",
											"Eating " + villagers [currentHouse].charms [2].myname + "? Nah bruh.",
											"I feel like " + villagers [currentHouse].charms [2].myname + " before bedtime is just a wishful fantasy.",
											villagers [currentHouse].charms [2].myname + "? Gross and ineffective, i'd say."
								};
										ask (foodResponses, 2);
										askFlag = true;
										break;	// case food break
									default:
										break;	// default case break
									}
								} while (!askFlag);
								talkFlag = true;
								break;	// case ask break
							case "recommend":
							//list allcharms
								bool recFlag = false;
								int slot = 0;
								int value = 0;
								float posit = 0f;
								do {
									QueueUpdate ("Recommend which charm?");
									int n = 1;
									foreach (Charm[] charms in allCharms) {
										foreach (Charm charmThing in charms) {
											QueueUpdate (n + "-" + charmThing.myname);
											n++;
										}
									}
									SendHelp ("Select charm #");
									yield return waitForCommand ();
									askRecommendCommand = TakeCommand ();
									int charmNumber;
									if (int.TryParse (askRecommendCommand, out charmNumber)) {
										if (charmNumber < 6) {	// door
											slot = 0;
											value = charmNumber - 1;
										} else if (charmNumber < 11) {	// body
											slot = 1;
											value = charmNumber - 6;
										} else if (charmNumber < 16) {	// eat
											slot = 2;
											value = charmNumber - 11;
										}
										recFlag = true;
									} else {
										QueueUpdate ("Sorry, which charm?");
									}
								} while (!recFlag);
								Charm selectedCharm = allCharms [slot] [value];
								if (villagers [currentHouse].getBelief (selectedCharm).positivity > 7f) {
									posit = 2f;
									QueueUpdate ("I've heard the same around town.");
									AddKnowledge (villagers [currentHouse].name + " respects your recommendation of " + selectedCharm.myname + ".");
								} else if (villagers [currentHouse].getBelief (selectedCharm).positivity < 3f) {
									posit = .5f;
									QueueUpdate ("Really? I've heard otherwise...");
									AddKnowledge (villagers [currentHouse].name + " suspects your recommendation of " + selectedCharm.myname + ".");
								} else {
									posit = 1f;
									QueueUpdate ("Hm, that could work.");
									AddKnowledge (villagers [currentHouse].name + " is neutral towards your recommendation of " + selectedCharm.myname + ".");
								}
								villagers [currentHouse].records.Add (new ShareRecord (player, selectedCharm, posit));
								talkFlag = true;
								break;	// case recommend break
							}
							dayFlag = true;
						} while (!talkFlag);
					} else {
						QueueUpdate ("You can't talk! You're a horrific exile!");
					}
					break;	// case talk break
				case "eavesdrop":
					bool eavesFlag = false;
					do {
						QueueUpdate ("Who are you eavesdropping on?");
						LookHouses ();
						SendHelp ("Enter House #");
						string thycommand;
						int result;
						do {
							yield return waitForCommand ();
							thycommand = TakeCommand();
						} while (thycommand == "" || !int.TryParse(thycommand, out result));
						splitCommand = thycommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
						currentHouse = IdentifyHouse (splitCommand [0]) - 1;
						if (villagers [currentHouse].isDead) {
							SendUpdate ("That villager is no more. Don't you remember the taste?");
						} else if (currentHouse > 0) {
							QueueUpdate ("You follow " + villagers [currentHouse].name + ", listening in to their conversations.");
							foreach (KeyValuePair<Villager, ShareRecord> kvpair in lastGossip) {
								if (villagers [currentHouse] == kvpair.Key) {
									// print out the interaction
									QueueUpdate (kvpair.Value.sharedWith.name + " listens intently as " + kvpair.Key.name + " recommends a " + kvpair.Value.recommended.myname + ".");
									AddKnowledge (kvpair.Key.name + " is telling others about " + kvpair.Value.recommended.myname + ".");
								}
							}
							eavesFlag = true;
							dayFlag = true;
						}
					
					} while (!eavesFlag);
					break;	// case eavesdrop break
				default:
					break;	// default case break
				}
			} while(!dayFlag);
			for (int j = 0; j < villagersRemaining; j++) {
				//villagers [i].ReselectCharms (allCharms);
			}
			yield return null;
		}
	}

	public List<KeyValuePair<Villager,ShareRecord>> lastGossip;

	public void DayStep ()
	{
		lastGossip = new List<KeyValuePair<Villager, ShareRecord>> ();
		for (int i = 0; i < villagersRemaining; i++) {
			int j = Random.Range (0, villagersRemaining - 1);
			if (j >= i) {
				j++;
			}
			Belief myShare = villagers [i].selectShare ();
			villagers [i].records.Add (new ShareRecord (villagers [j], myShare.charm, myShare.positivity));	// change, is this supposed to be a "j"
			lastGossip.Add (new KeyValuePair<Villager, ShareRecord> (villagers [i], new ShareRecord (villagers [j], myShare.charm, myShare.positivity)));	// same
			Belief toChange = villagers [j].getBelief (myShare.charm);
			toChange.positivity = (toChange.positivity * toChange.fervor + myShare.positivity * myShare.fervor) / (toChange.fervor + myShare.fervor);//consider sliding these values

		}
	}

	public void Talk (Villager victim)
	{
		Belief belief = victim.selectShare ();
		string[] stuffNugget = new string[]{
			" is a pretty popular protection practice.",
			" might be able to fend off the demon.",
			" hasn't been effective at protecting."
		};
		int reaction;
		if (belief.positivity > 7) {
			reaction = 0;	// positive
			AddKnowledge ("The town seems to think positively of " + belief.charm.myname + ".");
		} else if (belief.positivity < 3) {
			reaction = 2;	// negative
			AddKnowledge ("The town seems to think negatively of " + belief.charm.myname + ".");
		} else {
			reaction = 1;	// neutral
			AddKnowledge ("The town doesn't seem to know much about " + belief.charm.myname + ".");
		}
		QueueUpdate ("Word is that having a " + belief.charm.myname + stuffNugget [reaction]);
	}

	public void Gossip ()
	{
	}

	public void ask (string[] Responses, int charmSlot)
	{
		if (villagers [currentHouse].getBelief (villagers [currentHouse].charms [charmSlot]).positivity > 7) {
			QueueUpdate (Responses [Random.Range (0, 3)]);
			AddKnowledge (villagers [currentHouse].name + " believes strongly in their " + villagers [currentHouse].charms [charmSlot].myname + ".");
		} else if (villagers [currentHouse].getBelief (villagers [currentHouse].charms [charmSlot]).positivity < 3) {
			QueueUpdate (Responses [Random.Range (6, 9)]);
			AddKnowledge (villagers [currentHouse].name + " is uncertain of their " + villagers [currentHouse].charms [charmSlot].myname + ".");
		} else {
			QueueUpdate (Responses [Random.Range (3, 6)]);
			AddKnowledge (villagers [currentHouse].name + " has litle to say of their " + villagers [currentHouse].charms [charmSlot].myname + ".");
		}
	}
	
	int villagersRemaining = NUM_VILLAGERS;
	int currentHouse;
	int currentDepth;
	int housesVisited;
	bool isExiled = false;
	bool hasKilled;
	bool strangeForce;
	bool[] roomCharmPos;
	Charm leftCharm;
	int hunger = 0;
	int victim;

	#region night
	public IEnumerator Night ()
	{
		//check if hunger = 2: get batmanned. If hunger = 3, gameoverman.
		if (hunger == 2 && !isExiled) {
			// Do the exile scene and start day
			Exiled ();
			yield break;
		}
		string[] splitCommand = new string[]{"look"};
		currentDepth = -1;
		currentHouse = -1;
		housesVisited = 0;
		victim = -1;
		strangeForce = false;
		hasKilled = false;
		bool[] roomCharmPos = new bool[]{true, true, true};
		SendUpdate ("It is night, and the Hunger calls.");
		while (splitCommand[0]!="sleep") {
			if (splitCommand [0] == "leave") {
				if (housesVisited < 3) {
					switch (housesVisited) {
					case 1:
						QueueUpdate ("The moon has reached its peak.");
						break;
					case 2:
						QueueUpdate ("The moon is setting, and the sun is lightening the eastern sky.");
						break;
					}
					SendUpdate ("You have time to visit " + (3 - housesVisited).ToString () + " more houses this night.");
				} else if (housesVisited == 3 && !hasKilled) { 
					SendUpdate ("The sun is beginning to peek above the horizon. You must eat.");
				} else {
					break;	// lol
				}
			}
			switch (splitCommand [0]) {
			case "visit":
				if (currentDepth < 0) {
					try {
						Visit (splitCommand [1]);
					} catch (System.IndexOutOfRangeException e) {
						Debug.Log ("caught: " + e);
						SendUpdate ("What house do you want to visit?");
					}
				} else {
					SendUpdate ("You must leave the house before you can go to another.");
				}
				break;
			case "look":
				if (currentDepth == -1) {
					LookHouses ();
				} else if (currentDepth == 0) {
					QueueUpdate ("You are outside " + villagers [currentHouse].name + "'s house.");
				} else {
					seeCharm ();
				}
				break;
			case "proceed":
				if (strangeForce) {
					SendUpdate ("You cannot proceed any further.");
					break;
				}
				if (currentHouse == -1) {
					SendUpdate ("Proceed where?");
					break;
				}
				if (currentDepth < 3) {
					currentDepth ++;
				} else {
					QueueUpdate("You can proceed no further.");
					break;
				}
				if (currentDepth == 1) {
					QueueUpdate ("You enter through the doorway.");
				} else if (currentDepth == 2) {
					QueueUpdate ("You pass into the bedroom.");
				} else if (currentDepth == 3) {
					QueueUpdate ("You approach " + villagers [currentHouse].name + "'s bedside.");
				} else {
					SendUpdate ("I don't think it's supposed to do that...");
				}
				seeCharm ();
				roomCharmPos [currentDepth - 1] = false;
				if (currentDepth < 3) {
					doTurnBack ();
				}
				break;
			case "feast":
				if (currentDepth < 3) {
					SendUpdate ("You have yet to reach the sleeping villager...");
				} else if (hasKilled) {
					SendUpdate ("You have already eaten this night...");
				} else {
					Kill (currentHouse);
					splitCommand [0] = "leave";
				}
				break;
			case "leave":
				Leave ();
				break;
			default:
				SendUpdate ("What was that?");
				break;

			}
			string leaveOption = "";
			string proceedOption = "";
			string feastOption = "";
			string visitOption = "";
			if (currentDepth < 0) {
				visitOption = "\nvisit #";
			}
			if (currentDepth > -1) {
				leaveOption = "\nleave";
				if (currentDepth < 3) {
					proceedOption = "\nproceed";
				}
			}
			if (currentDepth == 3 && !hasKilled) {
				feastOption = "\nfeast";
			}
			SendHelp (visitOption + "\nlook" + leaveOption + proceedOption + feastOption + "\nsleep");
			string theircommand;
			do {
				yield return waitForCommand ();
				theircommand = TakeCommand ();
			} while (theircommand == "");
			splitCommand = theircommand.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries);
		}
		huntStatus ();
		updateKill (victim);
	}

	void huntStatus ()
	{
		if (hasKilled) {
			QueueUpdate ("Having made your kill, you hasten for home");
		} else {
			QueueUpdate ("You bear the hunger, but know you will be unable to go another night without.");//modify based on hunger
			hunger++;
		}
	}

	void seeCharm ()
	{
		string[] toSay = new string[]{
			"A " + villagers [currentHouse].charms [currentDepth - 1].myname + " is hung above the threshold.",
			"The sleeping villager clutches a " + villagers [currentHouse].charms [currentDepth - 1].myname + " to their chest.",
			"There is an odor of " + villagers [currentHouse].charms [currentDepth - 1].myname + " on their breath."
		};
		QueueUpdate (toSay [currentDepth - 1]);
		AddKnowledge (villagers [currentHouse].name + " has a " + villagers [currentHouse].charms [currentDepth - 1].myname + ".");
	}

	void Kill (int ded)
	{
		victim = ded;	// lol
		hasKilled = true;
		Villager pooSoul = villagers [ded];
		villagers [ded].isDead = true;
		villagersRemaining--;
		QueueUpdate (pooSoul.name + " makes a succulent banquet. The Hunger fades as you " +
			(housesVisited < 4 ? "remove the meaningless charms from the scene." : "flee the scene, snagging a few charms on your way out."));
		if (housesVisited == 4) {
			leftCharm = pooSoul.charms [Random.Range (0, 3)];
		} else {
			leftCharm = null;
		}
	}

	void updateKill (int victim)
	{
		if (victim > -1) {
			Villager deadGuy = villagers [victim];
			villagers.RemoveAt (victim);
			villagers.Add (deadGuy);
		}
	}

	void Visit (string arg)
	{
		if (IdentifyHouse (arg) > 0) {
			currentHouse = IdentifyHouse (arg) - 1;

			currentDepth = 0;
			QueueUpdate ("You arrive at " + villagers [currentHouse].name + "'s house.");

			housesVisited++;
		} else {
			QueueUpdate ("There is no villager of name " + arg + ".");
		}
	}

	bool CheckIsReal (string arg)
	{
		bool isReal = false;
		foreach (Villager villager in villagers) {
			if (villager.name.ToLower () == arg) {
				isReal = true;
			}
		}
		return isReal;
	}

	void doTurnBack ()
	{
		if (TurnedBack (currentHouse, currentDepth)) {
			QueueUpdate ("A strange force prevents you from going further...");
			strangeForce = true;
		}
	}

	void Leave ()
	{
		GiveBeliefs ();
		QueueUpdate ("You turn away from " + villagers [currentHouse].name + "'s house.");
		currentHouse = -1;
		currentDepth = -1;
		strangeForce = false;
	}

	void GiveBeliefs ()
	{
//		villagers [currentHouse].believes.Add (new Belief (villagers [currentHouse], roomCharmPos [0], villagers [currentHouse].charms [0], false));
//		villagers [currentHouse].fervor += 2;
//		if (!roomCharmPos [0]) {
//			villagers [currentHouse].believes.Add (new Belief (villagers [currentHouse], roomCharmPos [1], villagers [currentHouse].charms [1], false));
//			villagers [currentHouse].fervor += 2;
//			if (!roomCharmPos [1]) {
//				villagers [currentHouse].believes.Add (new Belief (villagers [currentHouse], roomCharmPos [2], villagers [currentHouse].charms [2], false));
//				villagers [currentHouse].fervor += 2;
//			}
//		}
	}

	void LookHouses ()
	{
		SendUpdate ("House " + (1) + ": " + villagers [0].name);
		for (int i=1; i<NUM_VILLAGERS; i++) {
			string isDed = "";
			if (villagers [i].isDead) {
				isDed = "*DECEASED* ";
			}
			QueueUpdate (isDed + "House " + (i + 1) + ": " + villagers [i].name);
		}
	}

	void Exiled ()
	{
		isExiled = true;
		Villager poorSod = villagers [Random.Range (0, villagersRemaining)];
		updateKill (villagers.IndexOf (poorSod));
		QueueUpdate ("The suspicion caught on. As you prepare to leave for another night of feasting, the door is suddenly swung open!");
		SendUpdate ("'I found you, monster!' " + poorSod + " exclaims! Now the town knows you for what you are!");
		QueueUpdate ("Before they can utter another word you have devoured them. But their final words unnerve you. You will have to be careful from here on out.");
	}

	#endregion

	int IdentifyHouse (string arg)
	{
		int house;
		if (int.TryParse (arg, out house)) {
			return house;
		} else {
			for (int i = 0; i < villagers.Count; i++) {
				if (villagers [i].name.Split (new char[0], System.StringSplitOptions.RemoveEmptyEntries) [0] == arg) {
					return i;
				}
			}
		}
		Debug.LogError ("invalid name");
		return 0;
	}

	public bool TurnedBack (int house, int depth)
	{
		return villagers [house].charms [depth] == allCharms [depth] [0];//check against real charms
	}

	public IEnumerator GameOver ()
	{
		if (hunger == 3) {
			SendUpdate ("The endless Hunger tears at your flesh, devouring at even your bones. You slump to the ground, unable to raise. Your vision fades...");
		} else {
			SendUpdate ("You stroll down the empty roads of your village. Each building houses depraved memories of a meal. Though you call this place your home, it is time to move on. The Hunger is, of course, never sated.");
		}
		yield return null;
	}

	public void SendHelp (string help)
	{
		manager.OnSendHelp (help);
	}

	public void AddKnowledge (string learned)
	{
		manager.OnLearnAdd (learned + "\n");
	}

	string[] firstNames = new string[]{
		"Adam", "Bob", "Roober", "Amelia", "Riley", "Leftwords", "Elliot", "Kyle", "George", "Kimber","Ashley", "Mike", "Herbert",
		"Paul", "Mathias", "Nicolas", "Peter", "Eric", "Catherine", "Russell", "Avorell", "Delliah", "Chester", "Mark", "Curly"
	};
	string[] lastNames = new string[]{
		"Scheudles", "Johanneson", "Ashworth", "Fungleston", "Michaelson", "George", "Preston", "Fodsbottom", "Tafte", "King", 
		"Ringly", "Locke", "Briar", "Crowbane", "Sheffield", "Milner", "Balner", "Mosswick", "Mockres", "Stanika", "Grendel", "Fishers", "Stonewall"
	};
	string[] doorCharms = new string[] {
		"Goat's Head", "Talisman", "Holy Sword", "Prayer Rope", "Happy B-Day Sign", "Nail from a Saint's Coffin", "Silver Bells", "Mistletoe",
		"Spiced Wine", "Lit Censer"
	};
	string[] bodyCharms = new string[] {
		"Rabbit's Foot", "Holy Water Vial", "Crystal Necklace", "Silver Cross", "Pentagram Pendant", "Rag Soaked in Virgin Blood", "Golden Locket",
		"Pouch of Holy Mother's Ashes", "Blessed Phylactery", "Holy Book"
	};
	string[] eatCharms = new string[] {
		"Garlic", "Crickets", "Iron Filings", "Quicksilver", "Unleavened Bread", "Cicada Husk", "Peppermint Tea", "Parsley, Sage, Rosemary and Thyme",
		"Lamb's Blood", "Chicken Feet", "Iocane Powder"
	};
}
