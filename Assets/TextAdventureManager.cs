﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TextAdventureManager : MonoBehaviour {

	public EventSystem system;
	public InputField input;
	public Text text;
	public Text helpText;
	public Text knowledgeText;
	private BroadRunner runner;

	// Use this for initialization
	void Start () {
		runner = gameObject.AddComponent<BroadRunner> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnFinishCommand(){
		runner.takeNewCommand(input.text);
		input.text = "";
		if (!system.currentSelectedGameObject == input.gameObject) {
			system.SetSelectedGameObject (input.gameObject);
		}
		input.OnPointerClick (new PointerEventData(EventSystem.current));
	}

	public void OnFinishedUpdate(){
		text.text = runner.toPrint;
	}

	public void OnSendHelp(string toShow){
		helpText.text = toShow;
	}
	public void OnAddHelp(string toAdd){
		helpText.text += toAdd;
	}
	public void OnAddReset(){
		helpText.text = "";
	}

	public void OnSendLearn(string toShow){
		knowledgeText.text = toShow;
	}
	public void OnLearnAdd(string known){
		knowledgeText.text += known;
	}
	public void OnLearnReset(){
		knowledgeText.text = "";
	}

}
